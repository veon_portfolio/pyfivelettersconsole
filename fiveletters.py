import os
import random

def PrintABC(guessLetters, word):
    """Вывод алфавита"""

    for k in range(ord('а'), ord('я'), 11):
        print('\n  ', end='')
        for i in range(k, k+11):
            if chr(i) in guessLetters:
                if chr(i) in word:
                    print(chr(i).upper() + ' ', end='')
                else:
                    print('_ ', end='')
            elif (chr(i) != 'ѐ'):
                print(chr(i) + ' ', end='')
            else:
                break
            
def ChoiceWord():
    """Получение списка слов"""

    fileWords = open('words.txt', 'r', encoding="utf-8")
    wordsList = []

    for words in fileWords:
        wordsList.append(words.replace('\n','',1))

    fileWords.close()

    return wordsList

def Briefing():
    """Проведение инструктажа"""
    playerChoice = ''
    os.system('cls||clear')
    playerChoice = str(input('Нажмите ENTER, чтобы начать инструктаж или' + 
                             '\n      любую другую, чтобы отказаться\n\t\t Ответ: '))

    if playerChoice == '':

        with open('briefing.txt', 'r', encoding="utf-8") as fileBriefing:
            textBriefing = fileBriefing.read().splitlines()
        while True:
            for i in range(4):
                os.system('cls||clear')
                for j in range(15*i, (i+1) * 15):
                    print(textBriefing[j])
                playerChoice = str(input('\n\tНажмите ENTER, чтобы продолжить или' + 
                                                '\n    любую клавишу, чтобы прекратить инструктаж\n\t\tОтвет: '))
                if playerChoice != '':
                    break
            if playerChoice != '':
                break
        fileBriefing.close()


def ContinueGame():
    """Продолжение/прекращение игры"""
    
    playerChoice = ''
    while (
                (playerChoice != 'n') and (playerChoice != '0') and (playerChoice != 'н') and
                (playerChoice != 'y') and (playerChoice != '1') and (playerChoice != 'д')
        ):
            playerChoice = str(input('Хотите повторить?\n1/y/д - Да\n0/n/н - Нет\n\nОтвет: '))

    if ((playerChoice == 'n') or (playerChoice == '0') or (playerChoice == 'н')):
        return False
    else:
        return True
    
def InputWord(wordList, guessedLetters):
    """Ввод слова"""
    
    d = dict()
    letters = guessedLetters
    while True:
        printWord = input("Введите слово длинной 5 букв: ").lower()

        if len(printWord) != 5:
            print("Длинна слова ДОЛЖНА быть 5 букв!")
        elif printWord not in wordList:
            print("Такого слова нет, пробуйте ещё!")
        else:
            for i in range(5):
                if printWord[i] not in letters:
                    letters += printWord[i]
            break
    d['letters'] = letters
    d['word'] = printWord
    
    return d

def PrintWordsLines(word, wordsLines, inputCounter):
    """Вывод текущего состояния слов"""
    for i in range(6):
        outLine = '_____'
        for j in range(5):
            if wordsLines[i][j] in word:
                if wordsLines[i][j] == word[j]:
                    outLine = outLine[:j] + wordsLines[i][j].upper() + outLine[j+1:]
                else:
                    outLine = outLine[:j] + wordsLines[i][j] + outLine[j+1:]
        print(outLine, end='')
        if inputCounter-1 == i:
            print (' *', end='')
        print()

def GameFiveGuesses():
    """Игровая функция"""

    Briefing()

    wordList = ChoiceWord()
    playerChoice = True
    
    while playerChoice:
        word = random.choice(wordList)
        wordsLines = ['_____','_____','_____','_____','_____', '_____']
        guessedLetters = ''
        inputCounter = 0
        d = dict()

        while True:
            os.system('cls||clear')
            printWord = ''

            PrintWordsLines(word, wordsLines, inputCounter)

            if word in wordsLines:
                print('Вы отгадали слово: ' + word.upper() + '\nПоздравляю!\n')
                break
            else:
                print('\nДопустимые буквы:', end='')
                PrintABC(guessedLetters, word)
                print('\n')

                d = InputWord(wordList, guessedLetters)
                printWord = d['word']
                guessedLetters = d['letters']

            wordsLines[inputCounter] = printWord
            inputCounter += 1
            if (inputCounter == 6):
                break

        playerChoice = ContinueGame()

    print("Возвращайтесь играть снова!")

#-----------------------------MAIN-----------------------------#
if __name__ == '__main__':
    GameFiveGuesses()
<h1 align="center">PyFiveLettersConsole</h1>

<p align="center">
	<img src="/images/fl2.png?cachefix" />
</p>

### Описание[RU]:
Консольная верси игры в "5 букв". Пользователю даётся 6 попыток отгадать слово, состоящее из 5 букв без повторений. Слова заранее вписаны в текстовый документ.

### Description[EN]:
Console version of the game "Wordle". The user is given 6 attempts to guess a word consisting of 5 letters without repetition. The words are pre-written into the text document.

### System requirements: 
- Python 3.6 and upper

### Как запустить[RU] / How to use[EN]:
 ```bash
git clone https://gitlab.com/veon_portfolio/pyfivelettersconsole.git
cd pyfivelettersconsole
python3 fiveletters.py 
```

### Как играть[RU] / How to play [EN]:
[RU] При первом запуске код спросит о желании пройти инструктаж, в котором описаны основные аспекты
игры и как понять, подходит буква или нет.

[EN] When you run it for the first time, the code will ask if you want to undergo training, which describes the main aspects
games and how to understand whether a letter fits or not.
<p align="center">
	<img src="/images/fl1.png?cachefix" />
</p>

[RU] При угадывании позиции буквы, она отобразится в строке в верхнем регистре. Если буква есть в загаданном слове, но
не в той позиции, она отобразится в нижнем регистре. Угаданные буквы отображаются в верхнем регистре алфавита. 
Буквы, введённые однажды, но отсутствующие в загаданном слове, перестают отображаться в алфавите.
Послежнее введённое слово отображается "звездочкой" на строках.

[EN] When you guess the position of a letter, it will appear on the line in uppercase. If the letter is in the hidden word, but
in the wrong position, it will appear in lower case. The guessed letters are displayed in upper case of the alphabet.
Letters that are entered once, but are missing from the hidden word, no longer appear in the alphabet.
The last word entered is displayed as an asterisk on the lines.
<p align="center">
	<img src="/images/fl3.png?cachefix" />
</p>

[RU] При угадывании целого слова, код спросит о повторном прохождении игры.

[EN] If you guess the whole word, the code will ask you to replay the game.
<p align="center">
	<img src="/images/fl4.png?cachefix" />
</p>

### Info:
- Support: blastyboom2001@gmail.com
<br />
(c) TheVeon